//
//  GPMetricDM.swift
//  
//
//  Created by Ivan Khvorostinin on 27/09/16.
//
//

import Foundation
import CoreData

extension GPMetricDM
{
    
    func setNext(_ next: GPMetricDM?)
    {
        if (next != nil)
        {
            nextID = next!.metricID
        }
        else
        {
            nextID = metricID
        }
    }
    
    func setNextNext(_ next: GPMetricDM?)
    {
        if (next != nil && next!.metricID == next!.nextID)
        {
            nextID = metricID
        }
        else if (next != nil)
        {
            nextID = next!.nextID
        }
        else
        {
            nextID = metricID
        }
    }

}

class GPMetricDM: NSManagedObject
{

    @NSManaged var metricID: Int16
    @NSManaged fileprivate(set) var nextID: Int16

}
