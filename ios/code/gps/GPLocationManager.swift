//
//  GPLocationManager.swift
//  Green Plane
//
//  Created by Ivan Khvorostinin on 23/09/16.
//  Copyright © 2016 Golden Flag America, LLC. All rights reserved.
//

import Foundation
import CoreLocation
import CoreGraphics
import GFUtils

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// MARK: - Plane maneuvers
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class DobStepChanger : NSObject
{
    
    fileprivate var _value: Double
    fileprivate let _delta: Double
    fileprivate let _shift: Double
    fileprivate let _reslt: Double
    fileprivate var _done = false
    
    init(value: Double, delta: Double, shift: Double)
    {
        _value = value
        _delta = delta
        _shift = shift
        _reslt = value + delta
    }
    
    var done: Bool
        {
        get
        {
            return _done
        }
    }
    
    var value: Double
        {
        get
        {
            return _value
        }
    }
    
    func change() -> Bool
    {
        if (done)
        {
            return false
        }
        
        if (_reslt > _value && _value + _shift > _reslt)
        {
            _value = _reslt
            _done = true
        }
        
        if (_reslt < _value && _value + _shift < _reslt)
        {
            _value = _reslt
            _done = true
        }
            
        else if (abs(_reslt - _value) < abs(_shift) * 1.5)
        {
            _value = _reslt
            _done = true
        }
            
        else
        {
            _value += _shift
        }
        
        return true
    }
    
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// MARK: - GPLocationManagerDelegate
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

protocol GPLocationManagerDelegate : NSObjectProtocol
{
    func locationManager(_ manager: GPLocationManager,
                         didUpdateLocations locations: [CLLocation])

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// MARK: - GPLocationManagerDelegate optional members
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

extension GPLocationManagerDelegate
{
    func locationManager(_ manager: GPLocationManager,
                         didUpdateLocations locations: [CLLocation])
    {
        // optional
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// MARK: - GPLocationManager
// Now simulates location changes with Timer and Random
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class GPLocationManager
{

    fileprivate var planeTimer: Timer?

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: - SAMPLE
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    var polygon: GFPolygon = []
    {
        didSet
        {
            rect = GFMath.Rect(polygon)
        }
    }
    
    // aircraft moves per second
    static let ticks = Double(60)
    
    // aircraft speed up
    static let boost = Double(5)
    
    // aircraft speed
    static let speed = Double(50)

    // 1609 - meters in mile
    // 60 - minutes in hour
    // 60 seconds in minute
    // 100 - timer interval compensation
    static let speedShift = speed * 1609 / 60 / 60 / ticks * boost
    
    // first turn angle per second
    static let turnAngle1 = Double(6)
    
    // first turn angle per timer tick
    static let turnShift1 = turnAngle1 / ticks * boost

    // first turn angle per second
    static let turnAngle2 = Double(5)

    // first turn angle per timer tick
    static let turnShift2 = turnAngle2 / ticks * boost

    fileprivate var rect = CGRect.zero
    fileprivate var location = CLLocation()
    fileprivate var turn1: DobStepChanger?
    fileprivate var turn2: DobStepChanger?
    fileprivate var move1: DobStepChanger?
    fileprivate var td: Double = 1 // turn direction
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: - Public fields
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    weak var delegate: GPLocationManagerDelegate?
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: - Functions
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    func startUpdatingLocation()
    {
        assert(planeTimer == nil)
        
        arc4random();
        
        planeTimer = Timer.scheduledTimer(timeInterval: Double(1) / GPLocationManager.ticks,
            target: self,
            selector: #selector(_locationChanged),
            userInfo: nil,
            repeats: true)
        
        let rect = GFMath.Rect(polygon)
        let origin = CGPoint(x: rect.origin.x, y: rect.origin.y)
        
        location = CLLocation(coordinate: CLLocationCoordinate2D(cg: origin),
                              course: 90)
    }
    
    func stopUpdatingLocation()
    {
        assert(planeTimer != nil)
        
        planeTimer?.invalidate()
        planeTimer = nil
    }
    
    fileprivate func _locationMove(_ location: CLLocation, course: CLLocationDirection) -> CLLocation
    {
        return CLLocation(coordinate: location.coordinate.shift(GPLocationManager.speedShift, course: course),
                          course: course)
    }
    
    @objc fileprivate func _locationChanged()
    {
        var location = _locationMove(self.location, course: self.location.course)

        if (turn1 != nil && turn1!.change())
        {
            location = location.cloneWithCourse(turn1!.value)
        }
        
        else if (turn2 != nil && turn2!.change())
        {
            location = location.cloneWithCourse(turn2!.value)
        }
            
        else if (move1 != nil && move1!.change())
        {
            // we already moved at the beginning of method
        }
        
        else if (false == rect.contains(location.cgpoint))
        {
            turn1 = DobStepChanger(value: location.course,
                                   delta: 90 * td,
                                   shift: GPLocationManager.turnShift1 * td)
        }
        
        if (turn1 != nil && turn1!.done)
        {
            turn1 = nil
            turn2 = DobStepChanger(value: location.course,
                                   delta: -270 * td,
                                   shift: -GPLocationManager.turnShift2 * td)
        }
        
        if (turn2 != nil && turn2!.done)
        {
            // move 1 mile
            
            turn2 = nil
            move1 = DobStepChanger(value: 0, delta: 1609, shift: GPLocationManager.speedShift)
        }
        
        if (move1 != nil && move1!.done)
        {
            td *= -1
            move1 = nil
        }
        
        self.location = location
        delegate?.locationManager(self, didUpdateLocations: [ location ])
    }
}
