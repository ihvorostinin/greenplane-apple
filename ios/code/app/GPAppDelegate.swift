//
//  AppDelegate.swift
//  Green Plane
//
//  Created by Ivan Khvorostinin on 12/09/16.
//  Copyright © 2016 Golden Flag America, LLC. All rights reserved.
//

import UIKit
import MagicalRecord

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate, UISplitViewControllerDelegate
{

    var window: UIWindow?
    var main: GPMainController?
    var gps = GPLocationManager()

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
    {
        // Library path
        
        NSLog("%@", NSSearchPathForDirectoriesInDomains(.libraryDirectory, .userDomainMask, true)[0])
        
        // init DB
        
        NSManagedObjectContext.mr_initializeDefaultContext(with: GPMainDB.shared.persistentStoreCoordinator)
        MagicalRecord.setLoggingLevel(MagicalRecordLoggingLevel.off)

        // show Main
        
        let mainView = window!.rootViewController as! GPMainViewController

        mainView.loadViewIfNeeded()
        main = GPMainController(view: mainView)
        
        main?.metrics.load()
        main!.show()
        
        // init GPS
        
        gps.polygon = main!.map.field.polygon

        gps.delegate = main!.map
        gps.startUpdatingLocation()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication)
    {
    }

    func applicationDidEnterBackground(_ application: UIApplication)
    {
    }

    func applicationWillEnterForeground(_ application: UIApplication)
    {
    }

    func applicationDidBecomeActive(_ application: UIApplication)
    {
    }

    func applicationWillTerminate(_ application: UIApplication)
    {
        GPMainDB.shared.saveContext()
    }
    
    func application(_ application: UIApplication, shouldSaveApplicationState coder: NSCoder) -> Bool
    {
        return true
    }
    
    func application(_ application: UIApplication, shouldRestoreApplicationState coder: NSCoder) -> Bool
    {
        return true
    }

}

