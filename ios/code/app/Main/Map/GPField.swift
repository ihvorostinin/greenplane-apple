//
//  GPField.swift
//  Green Plane
//
//  Created by Ivan Khvorostinin on 22/09/16.
//  Copyright © 2016 Golden Flag America, LLC. All rights reserved.
//

import CoreGraphics
import GFUtils

class GPField
{
    // CGPoint: x - latitude, y - longitude

    var name: String = ""
    var polygon: GFPolygon = []
    var sprayed: Array<GFPolygon> = []
}
