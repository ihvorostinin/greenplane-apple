//
//  GPMapViewProtocol.swift
//  Green Plane
//
//  Created by Ivan Khvorostinin on 22/09/16.
//  Copyright © 2016 Golden Flag America, LLC. All rights reserved.
//

import CoreLocation

protocol GPMapViewProtocol : NSObjectProtocol
{
    
    func show(_ field: GPField)
    func show(_ plane: CLLocation)
    
}
