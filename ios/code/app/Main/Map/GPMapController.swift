//
//  GPMapController.swift
//  Green Plane
//
//  Created by Ivan Khvorostinin on 22/09/16.
//  Copyright © 2016 Golden Flag America, LLC. All rights reserved.
//

import Foundation
import CoreGraphics
import CoreLocation
import GFUtils

class GPMapController
    : NSObject
    , GPLocationManagerDelegate
{
    
    fileprivate var view: GPMapViewProtocol
    
    let field = GPField()

    init(view: GPMapViewProtocol)
    {
        self.view = view
        
        // init data stub
        
        field.name = "The best field \"Red-haired witch\""
        
        // area
        
        field.polygon.append(CGPoint(x: 32.113650, y: -99.228067))
        field.polygon.append(CGPoint(x: 32.113650, y: -99.219224))
        field.polygon.append(CGPoint(x: 32.110090, y: -99.219224))
        field.polygon.append(CGPoint(x: 32.110291, y: -99.228167))
        
        // sprayed "O"
        
        var sprayedO = GFPolygon()
        
        sprayedO.append(CGPoint(x: 32.113617, y: -99.226279))
        sprayedO.append(CGPoint(x: 32.112582, y: -99.225221))
        sprayedO.append(CGPoint(x: 32.111598, y: -99.224923))
        sprayedO.append(CGPoint(x: 32.110310, y: -99.226651))
        sprayedO.append(CGPoint(x: 32.111459, y: -99.227799))
        sprayedO.append(CGPoint(x: 32.112304, y: -99.227799))
        
        field.sprayed.append(sprayedO)
        
        // sprayed "K"
        
        var sprayedK = GFPolygon()
        
        sprayedK.append(CGPoint(x: 32.113314, y: -99.221525))
        sprayedK.append(CGPoint(x: 32.113201, y: -99.219543))
        sprayedK.append(CGPoint(x: 32.111837, y: -99.221197))
        sprayedK.append(CGPoint(x: 32.110297, y: -99.219274))
        sprayedK.append(CGPoint(x: 32.110600, y: -99.222106))
        
        field.sprayed.append(sprayedK)
    }
    
    func show()
    {
        view.show(field)
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: - GPLocationManager
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    func locationManager(_ manager: GPLocationManager,
                         didUpdateLocations locations: [CLLocation])
    {
        view.show(locations.last!)
    }
    
}
