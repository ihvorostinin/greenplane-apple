//
//  DetailViewController.swift
//  Green Plane
//
//  Created by Ivan Khvorostinin on 12/09/16.
//  Copyright © 2016 Golden Flag America, LLC. All rights reserved.
//

import UIKit
import MapKit
import GFUtils

class GPMapViewController
    : UIViewController
    , MKMapViewDelegate
    , GPMapViewProtocol
{

    var field: GPField!
    
    var plane = false
    var planeLocation: CLLocation!
    var planeCourse = CLLocationDirection()
    
    var polygon: MKPolygon?
    var sprayed: Array<MKPolygon> = []
    
    @IBOutlet fileprivate weak var mapView: MKMapView!
    @IBOutlet fileprivate weak var mapTap: UITapGestureRecognizer!
    @IBOutlet fileprivate weak var typeSwitch: UISegmentedControl!
    @IBOutlet weak var navMore: UIBarButtonItem!

    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: - Map utils
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    fileprivate func _applyTypeFromSwitch()
    {
        switch typeSwitch.selectedSegmentIndex
        {
        case 0:
            mapView.mapType = .standard
            break
        case 1:
            mapView.mapType = .satellite
            break
        case 2:
            mapView.mapType = .hybrid
            break
        default:
            assert(false);
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: - Actions
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @IBAction fileprivate func typeChanged(_ sender: AnyObject)
    {
        _applyTypeFromSwitch()
    }

    @IBAction fileprivate func mapTapped(_ sender: AnyObject)
    {
        let point = mapTap.location(in: mapView)
        let coord = mapView.convert(point, toCoordinateFrom: mapView)
        
        NSLog("tap %f, %f", coord.latitude, coord.longitude)
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: - Data display
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    fileprivate func _showField()
    {
        // show field
        
        mapView.setRegion(MKCoordinateRegion(polygon: field.polygon), animated: true)
        
        // add annotation at center
        
        mapView.addAnnotation(GFMapAnnotation(center: field.polygon, title: field.name))
        
        // add field overlay

        polygon = MKPolygon(gf: field.polygon)
        mapView.add(polygon!)
        
        // add sprayed area
        
        for i in field.sprayed
        {
            sprayed.append(MKPolygon(gf: i))
            mapView.add(sprayed.last!)
        }
    }
    
    fileprivate func _movePlane()
    {
        assert(abs(Int32(planeLocation.coordinate.latitude)) != 0)
        assert(abs(Int32(planeLocation.coordinate.longitude)) != 0)
        
        if (view.window == nil)
        {
            return
        }
        
        mapView.camera.centerCoordinate = planeLocation.coordinate
        mapView.camera.heading = planeLocation.course
        
        // hardcode pitch and altitude
        // pitch available only in "Standard" camera mode
        
        mapView.camera.pitch = 30 // in degrees to normal (vertical)
        mapView.camera.altitude = 1000 // in meters
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: - UIViewController
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        // apply switch
        
        typeSwitch.selectedSegmentIndex = 1
        typeSwitch.translatesAutoresizingMaskIntoConstraints = true
        self.navigationItem.titleView = typeSwitch
        
        _applyTypeFromSwitch()
        
        // show field
        
        if (field != nil)
        {
            _showField()
        }
        
        // show plane
        
        if (plane)
        {
            _movePlane()
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: - GPMapViewProtocol
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    func show(_ field: GPField)
    {
        self.field = field
        
        if (mapView != nil)
        {
            _showField()
        }
    }
    
    func show(_ plane: CLLocation)
    {
        self.plane = true
        self.planeLocation = plane
        
        if (mapView != nil)
        {
            _movePlane()
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: - MKMapViewDelegate
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer
    {
        if (overlay === polygon)
        {
            let render = MKPolygonRenderer(overlay: overlay)
            
            render.strokeColor = UIColor.yellow
            render.lineWidth = 2
            
            return render
        }
        
        if (overlay is MKPolygon && sprayed.contains(overlay as! MKPolygon))
        {
            let render = MKPolygonRenderer(overlay: overlay)
            
            render.fillColor = UIColor(red: 1, green: 0, blue: 0, alpha: 0.3)
            render.strokeColor = UIColor.blue
            render.lineWidth = 1
            
            return render
        }
        
        assert(false)
        
        return MKOverlayRenderer()
    }
    
}

