//
//  GPMainController.swift
//  Green Plane
//
//  Created by Ivan Khvorostinin on 24/09/16.
//  Copyright © 2016 Golden Flag America, LLC. All rights reserved.
//

import Foundation

class GPMainController
    : NSObject
{
    
    var map: GPMapController!
    var metrics: GPMetricsController!

    init(view: GPMainViewController)
    {
        metrics = GPMetricsController(view: view.metricsView!)
        map = GPMapController(view: view.mapView!)
    }
    
    func show()
    {
        map.show()
        metrics.show()
    }
    
}
