//
//  GPMetricsViewController.swift
//  Green Plane
//
//  Created by Ivan Khvorostinin on 14/09/16.
//  Copyright © 2016 Golden Flag America, LLC. All rights reserved.
//

import Foundation
import UIKit
import GFUtils
import MagicalRecord

class GPMetricsController
    : NSObject
{
    
    var view: GPMetricsViewProtocol
    var data: Array<GPMetric> = []

    init(view: GPMetricsViewProtocol)
    {
        self.view = view

        // init data stub
        // !!! ids should start from 0
        // !!! ids increment should be 1
        // if remove/add you should migrate DB
        
        var metric: GPMetric?
        
        metric = GPMetric(id: 0)
        metric!.icon = UIImage(named: "MetricsIconPlaceholder")
        metric!.info.append(GFPair(key: "Speed", val: "51 MPH"))
        metric!.info.append(GFPair(key: "Altitude", val: "45 Ft"))
        metric!.info.append(GFPair(key: "Heading", val: "360.0"))
        data.append(metric!)

        for i in 1 ..< 50
        {
            metric = GPMetric(id: i)
            metric!.icon = UIImage(named: "MetricsIconPlaceholder")
            metric!.info.append(GFPair(key: "Name", val: "Val " + String(i)))
            data.append(metric!)
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: - DB
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    fileprivate func _orderLoad() -> Dictionary<Int, Int>
    {
        let metrics = GPMetricDM.mr_findAll()
        
        if (metrics?.count == 0)
        {
            return Dictionary<Int, Int>()
        }
        
        var result = Dictionary<Int, Int>() // mertic ID - index
        var map = Dictionary<Int, Int>() // metric ID - next metric ID
        var metricID: Int

        // store to map for convenience
        
        for i in metrics!
        {
            let dm = i as! GPMetricDM
            
            map[Int(dm.metricID)] = Int(dm.nextID)
        }
        
        // find first element
        
        var mapCopy = map
        
        for (k, v) in map
        {
            if (k == v)
            {
                continue
            }
            
            mapCopy.removeValue(forKey: v)
        }
        
        // db broken
        
        if (mapCopy.count != 1)
        {
            NSLog("Metrics order DB broken (first elements count: %d). Deleted all", mapCopy.count)
            
            return Dictionary<Int, Int>()
        }
        
        // calc order
        
        metricID = mapCopy.first!.0
        
        for i in 0 ..< metrics!.count
        {
            result[metricID] = i
            metricID = map[metricID]!
        }
        
        return result
    }
    
    fileprivate func _orderInit(_ metrics: Array<GPMetric>)
    {
        MagicalRecord.save (
        { (context) in
            
            GPMetricDM.mr_truncateAll(in: context)

            var prev: GPMetricDM?
            
            for i in metrics
            {
                let dm = GPMetricDM.mr_createEntity(in: context)!
                
                dm.metricID = Int16(i.ID)
                dm.setNext(nil)
                
                prev?.setNext(dm)
                prev = dm
            }
        })
    }
    
    fileprivate func _orderUpdate(_ metricID: Int, afterID: Int)
    {
        MagicalRecord.save (
        { (context) in

            let metric     = GPMetricDM.mr_findFirst(with: NSPredicate(format: "metricID = %d", metricID),
                                                                  in:context)
            let metricPrev = GPMetricDM.mr_findFirst(with: NSPredicate(format: "nextID = %d", metricID),
                                                                  in:context)
            let target     = GPMetricDM.mr_findFirst(with: NSPredicate(format: "metricID = %d", afterID),
                                                                  in:context)

            metricPrev?.setNextNext(metric)
            metric?.setNextNext(target)
            target?.setNext(metric)
        })
    }

    fileprivate func _orderUpdate(_ metricID: Int, beforeID: Int)
    {
        MagicalRecord.save (
        { (context) in
                
            let metric     = GPMetricDM.mr_findFirst(with: NSPredicate(format: "metricID = %d", metricID),
                                                     in:context)
            let metricPrev = GPMetricDM.mr_findFirst(with: NSPredicate(format: "nextID = %d", metricID),
                                                     in:context)
            let target     = GPMetricDM.mr_findFirst(with: NSPredicate(format: "metricID = %d", beforeID),
                                                     in:context)
            let targetPrev = GPMetricDM.mr_findFirst(with: NSPredicate(format: "nextID = %d", beforeID),
                                                     in:context)
            
            metricPrev?.setNextNext(metric)
            metric?.setNext(target)
            targetPrev?.setNext(metric)
        })
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: - Callbacks
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    fileprivate func _reorderAfter(_ metric: GPMetric, after: GPMetric)
    {
        _orderUpdate(metric.ID, afterID: after.ID)
    }

    fileprivate func _reorderBefore(_ metric: GPMetric, before: GPMetric)
    {
        _orderUpdate(metric.ID, beforeID: before.ID)
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: - Actions
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    func load()
    {
        // loading such small scope of data from DB is very fast. So we can do it on Main thread
        
        let order = _orderLoad()
        var reset = false
        
        // check order valid
        
        if (false == reset && order.count == 0)
        {
            reset = true
        }
        
        if (false == reset && order.count != data.count)
        {
            reset = true
        }
        
        if (reset)
        {
            _orderInit(data)
            return
        }

        // sort
        
        data.sort { return order[$0.ID]! < order[$1.ID]! }
    }
    
    func show()
    {
        view.onReorderedAfter(_reorderAfter)
        view.onReorderedBefore(_reorderBefore)
        view.show(data)
    }

}
