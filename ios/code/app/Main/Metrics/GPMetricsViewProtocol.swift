//
//  GPMetricsProtocol.swift
//  Green Plane
//
//  Created by Ivan Khvorostinin on 14/09/16.
//  Copyright © 2016 Golden Flag America, LLC. All rights reserved.
//

import Foundation

typealias GPMetricReorder = (_ metric: GPMetric, _ target: GPMetric) -> Void

protocol GPMetricsViewProtocol : NSObjectProtocol
{

    func show(_ metrics: Array<GPMetric>)
    
    func onReorderedAfter(_ callback: @escaping GPMetricReorder)
    func onReorderedBefore(_ callback: @escaping GPMetricReorder)
    
}
