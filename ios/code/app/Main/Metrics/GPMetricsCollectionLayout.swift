//
//  GPMetricsCollectionLayout.swift
//  Green Plane
//
//  Created by Ivan Khvorostinin on 23/09/16.
//  Copyright © 2016 Golden Flag America, LLC. All rights reserved.
//

import Foundation
import UIKit

class GPMetricsCollectionViewLayout : UICollectionViewFlowLayout
{
    
    var cellTransform = CGAffineTransform.identity
    var cellIndexPath: IndexPath?
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes?
    {
        let attributes = super.layoutAttributesForItem(at: indexPath)?.copy()
            as! UICollectionViewLayoutAttributes
        
        if (cellIndexPath == indexPath)
        {
            attributes.transform = cellTransform
        }
        
        return attributes
    }
    
}
