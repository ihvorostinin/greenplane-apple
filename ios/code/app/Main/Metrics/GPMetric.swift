//
//  GPMetric.swift
//  Green Plane
//
//  Created by Ivan Khvorostinin on 14/09/16.
//  Copyright © 2016 Golden Flag America, LLC. All rights reserved.
//

import Foundation
import UIKit
import GFUtils

class GPMetric
{
    
    let ID: Int // initial order num (unique)
    var icon: UIImage?
    var info: Array<GFPair<String, String>> = []
    
    init(id: Int)
    {
        ID = id
    }
    
}
