//
//  CollectionController.swift
//  Green Plane
//
//  Created by Ivan Khvorostinin on 16/09/16.
//  Copyright © 2016 Golden Flag America, LLC. All rights reserved.
//

import Foundation
import UIKit
import GFUtils

class GPMetricsViewController
    : UICollectionViewController
    , UIPopoverPresentationControllerDelegate
    , UIGestureRecognizerDelegate
    , GPMetricsViewProtocol
{

    @IBOutlet fileprivate weak var longPress: UILongPressGestureRecognizer!
    @IBOutlet fileprivate weak var metricsLayout: GPMetricsCollectionViewLayout!

    fileprivate var pressed: UICollectionViewCell?

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: - Data
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    fileprivate var metrics: Array<GPMetric> = []
    fileprivate var reorderAfter: GPMetricReorder = {_,_ in }
    fileprivate var reorderBefore: GPMetricReorder = {_,_ in }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: - View
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    fileprivate func _showSelection(_ cell: UIView)
    {
        UIView.animate(withDuration: 0.5,
                                   delay: 0,
                                   options: UIViewAnimationOptions.allowUserInteraction,
                                   animations:
        {
            cell.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        },
                                   completion:nil);
    }

    fileprivate func _hideSelection(_ cell: UIView)
    {
        UIView.animate(withDuration: 0.5,
                                   delay: 0,
                                   options: UIViewAnimationOptions.allowUserInteraction,
                                   animations:
        {
            cell.backgroundColor = UIColor.clear
        },
                                   completion:nil);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: - Actions
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @IBAction fileprivate func longPress(_ sender: UILongPressGestureRecognizer)
    {
        // dragging cell jumps on start
        // i've tried to subclass layout (GPMetricsCollectionViewLayout)
        // but without success
        // will see it later :)
        
        let index = collectionView!.indexPathForItem(at: sender.location(in: collectionView!))
        
        if (index == nil)
        {
            return;
        }
        
        let cell  = collectionView!.cellForItem(at: index!)!
        
        if (sender.state == .began)
        {
            pressed = cell

            var offsetX: CGFloat = 7
            
            if (collectionView!.contentSize.width - cell.frame.origin.x - cell.frame.size.width < cell.frame.size.width)
            {
                offsetX *= -1
            }

            let transform = CGAffineTransform.identity.translatedBy(x: offsetX, y: -7)
            
            metricsLayout.cellIndexPath = index
            metricsLayout.cellTransform = transform
            
            UIView.animate(withDuration: 0.5,
                                       animations:
            {
                cell.backgroundColor = UIColor(white: 0, alpha: 0.3)
                cell.layer.setAffineTransform(transform)
            })
        }
        
        else if (sender.state != .changed)
        {
            metricsLayout.cellTransform = CGAffineTransform.identity

            UIView.animate(withDuration: 0.5,
                                       animations:
            {
                self.pressed?.backgroundColor = UIColor.clear
                cell.transform = CGAffineTransform.identity
            })
            
            pressed = nil
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: - GPMetricsViewProtocol
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    func show(_ metrics: Array<GPMetric>)
    {
        self.metrics = metrics
        self.collectionView?.reloadData()
    }
    
    func onReorderedAfter(_ callback: @escaping GPMetricReorder)
    {
        reorderAfter = callback
    }

    func onReorderedBefore(_ callback: @escaping GPMetricReorder)
    {
        reorderBefore = callback
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: - UIGestureRecognizerDelegate
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer,
                           shouldRecognizeSimultaneouslyWith
                           otherGestureRecognizer: UIGestureRecognizer) -> Bool
    {
        if (gestureRecognizer == longPress || otherGestureRecognizer == longPress)
        {
            return true
        }
        
        else
        {
            return false
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: - UICollectionViewDataSource
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    override func collectionView(_ collectionView: UICollectionView,
                                 numberOfItemsInSection section: Int) -> Int
    {
        return metrics.count
    }
    
    override func collectionView(_ collectionView: UICollectionView,
                                 cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        // fetch view and model
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "default",
                                                                          for: indexPath)
        let cellInfo = cell.contentView.subviews[1];
        
        let metric = metrics[indexPath.row]
        
        // util to apply model data to appropriate labels
        
        let metricInstall =
        { (model: GFPair<String, String>, view: UIView) in
                
            (view.subviews[0] as! UILabel).text = model.key;
            (view.subviews[1] as! UILabel).text = model.val;
            
            view.isHidden = false
        }
        
        // hide all info views first
        
        for i in cellInfo.subviews
        {
            i.isHidden = true
        }
        
        // apply and show info
        
        if (metric.info.count > 0)
        {
            metricInstall(metric.info[0], cellInfo.subviews[0]);
        }
        
        if (metric.info.count > 1)
        {
            metricInstall(metric.info[1], cellInfo.subviews[1]);
        }
        
        if (metric.info.count > 2)
        {
            metricInstall(metric.info[2], cellInfo.subviews[2]);
        }
        
        if (metric.info.count > 3)
        {
            // not all data shown
            assert(false)
        }
        
        // decorate
        
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.layer.borderWidth = 1
        
        // done
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView,
                                 moveItemAt sourceIndexPath: IndexPath,
                                 to destinationIndexPath: IndexPath)
    {
        if (destinationIndexPath.row > sourceIndexPath.row)
        {
            reorderAfter(metrics[sourceIndexPath.row], metrics[destinationIndexPath.row])

            metrics.insert(metrics[sourceIndexPath.row], at: destinationIndexPath.row + 1)
            metrics.remove(at: sourceIndexPath.row)
        }
        else
        {
            reorderBefore(metrics[sourceIndexPath.row], metrics[destinationIndexPath.row])

            metrics.insert(metrics[sourceIndexPath.row], at: destinationIndexPath.row)
            metrics.remove(at: sourceIndexPath.row + 1)
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: - UICollectionViewDelegate
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    override func collectionView(_ collectionView: UICollectionView,
                                 shouldSelectItemAt indexPath: IndexPath) -> Bool
    {
        let cell = collectionView.cellForItem(at: indexPath)

        if (cell!.isSelected)
        {
            return false
        }
        
        _showSelection(collectionView.cellForItem(at: indexPath)!)
        
        return true
    }
    
    override func collectionView(_ collectionView: UICollectionView,
                                 shouldDeselectItemAt indexPath: IndexPath) -> Bool
    {
        let cell = collectionView.cellForItem(at: indexPath)
        
        if (false == cell!.isSelected)
        {
            return false
        }
        
        _hideSelection(collectionView.cellForItem(at: indexPath)!)
        
        return true
    }
    
    override func collectionView(_ collectionView: UICollectionView,
                                 didSelectItemAt indexPath: IndexPath)
    {
        let details = storyboard?.instantiateViewController(withIdentifier: "MetricDetails")
     
        details!.modalPresentationStyle = UIModalPresentationStyle.popover
        present(details!, animated: true, completion: nil)
        
        let popover = details?.popoverPresentationController
        
        popover!.sourceView = collectionView.cellForItem(at: indexPath)
        popover!.sourceRect = popover!.sourceView!.bounds
        popover!.permittedArrowDirections = UIPopoverArrowDirection.left
        popover!.canOverlapSourceViewRect = false
        popover!.delegate = self
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: - UIPopoverPresentationControllerDelegate
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    func popoverPresentationControllerShouldDismissPopover(
        _ popoverPresentationController: UIPopoverPresentationController) -> Bool
    {
        let index = collectionView!.indexPathsForSelectedItems![0]
        let cell = collectionView?.cellForItem(at: index)
        
        collectionView?.deselectItem(at: index, animated: true)
        _hideSelection(cell!)
        return true;
    }
    
}
