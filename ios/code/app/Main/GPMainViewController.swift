//
//  GPMainViewController.swift
//  Green Plane
//
//  Created by Ivan Khvorostinin on 13/09/16.
//  Copyright © 2016 Golden Flag America, LLC. All rights reserved.
//

import UIKit

class GPMainViewController
    : UISplitViewController
    , UISplitViewControllerDelegate
{
    fileprivate static let MetricsWidthKey = "MetricsWidth"
    
    fileprivate static let BasementHeight: CGFloat  = 126
    fileprivate static let PrimaryMinWidth: CGFloat = 286
    fileprivate static let PrimaryMaxWidth: CGFloat = 426
    
    fileprivate static let ResizerWidth: CGFloat = 30
    fileprivate static let ResizerWidthHalf: CGFloat = ResizerWidth / 2

    fileprivate var metricsWidth = PrimaryMinWidth

    fileprivate var resizer: UIView = UIView()
    fileprivate var resizerLeft: NSLayoutConstraint?

    @IBOutlet fileprivate var navMetrics: UIBarButtonItem!
    @IBOutlet fileprivate var navMap: UIBarButtonItem!
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: - Child controllers
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    fileprivate var primary: UINavigationController
    {
        return viewControllers[0] as! UINavigationController
    }

    fileprivate var details: UINavigationController
    {
        return viewControllers[1] as! UINavigationController
    }

    fileprivate(set) var metricsView: GPMetricsViewController?
    fileprivate(set) var mapView: GPMapViewController?
    fileprivate var statusController: GPStatusViewController?

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: - Actions
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    @IBAction func onNavMap(_ sender: AnyObject)
    {
        showDetailViewController(mapView!, sender: self)
    }
    
    @IBAction func onNavMetrics(_ sender: AnyObject)
    {
        metricsView!.navigationItem.leftBarButtonItem = navMap
        
        primary.popViewController(animated: true)
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: - Resizer
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    fileprivate func _resizerUpdateVisibility(_ forMode: UISplitViewControllerDisplayMode)
    {
        let visible = forMode == .allVisible && traitCollection.horizontalSizeClass == .regular
        
        resizer.alpha = visible ? 1 : 0
        
        // animate appearance from left as Primary controller
        
        _resizerPosition(visible ? metricsWidth : 0)
    }
    
    fileprivate func _resizerPosition(_ width: CGFloat)
    {
        resizerLeft?.constant = width - GPMainViewController.ResizerWidthHalf
    }
    
    fileprivate func _resize(_ width: CGFloat)
    {
        minimumPrimaryColumnWidth = width
        maximumPrimaryColumnWidth = width
        
        _resizerPosition(width)
    }
    
    fileprivate func _resizeComplete(_ width: CGFloat)
    {
        var widthFinal = width
        
        if (fabs(width - GPMainViewController.PrimaryMinWidth) < fabs(width - GPMainViewController.PrimaryMaxWidth))
        {
            widthFinal = GPMainViewController.PrimaryMinWidth
        }
        else
        {
            widthFinal = GPMainViewController.PrimaryMaxWidth
        }
        
        UIView.animate(withDuration: 0.5, animations: {
            self._resize(widthFinal)
            self.view.layoutIfNeeded()
        })
        
        
        metricsWidth = widthFinal
    }
    
    @objc fileprivate func _resized(_ recognizer: UIPanGestureRecognizer)
    {
        var width = round(metricsWidth + recognizer.translation(in: nil).x)
        
        if (width > GPMainViewController.PrimaryMaxWidth)
        {
            width = GPMainViewController.PrimaryMaxWidth
        }
        
        if (width < GPMainViewController.PrimaryMinWidth)
        {
            width = GPMainViewController.PrimaryMinWidth
        }
        
        if (recognizer.state == UIGestureRecognizerState.changed)
        {
            _resize(width)
        }
            
        else if (recognizer.state == UIGestureRecognizerState.ended)
        {
            _resizeComplete(width)
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: - View
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    fileprivate func _updateMetricsTrigger(_ forMode: UISplitViewControllerDisplayMode)
    {
        if (forMode == .primaryHidden)
        {
            navMetrics.target = displayModeButtonItem.target
            navMetrics.action = displayModeButtonItem.action
            mapView?.navigationItem.leftBarButtonItem = navMetrics
        }
        else
        {
            mapView?.navigationItem.leftBarButtonItem = nil
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: - UIViewController
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    override func encodeRestorableState(with coder: NSCoder)
    {
        super.encodeRestorableState(with: coder)
        
        coder.encode(Float(metricsWidth), forKey: GPMainViewController.MetricsWidthKey)
    }

    override func decodeRestorableState(with coder: NSCoder)
    {
        super.decodeRestorableState(with: coder)
        
        metricsWidth = CGFloat(coder.decodeFloat(forKey: GPMainViewController.MetricsWidthKey))
        
        _resize(metricsWidth)
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        delegate = self

        // map
        
        metricsView = viewControllers[0].childViewControllers[0] as? GPMetricsViewController
        mapView = viewControllers[1].childViewControllers[0] as? GPMapViewController
        
        _updateMetricsTrigger(displayMode)

        // status

        statusController = storyboard?.instantiateViewController(withIdentifier: "GPStatusViewController")
            as? GPStatusViewController
        
        metricsView!.view.addSubview(statusController!.view)
        
        // resizer
        
        view.addSubview(resizer)

        resizer.backgroundColor = UIColor.clear//UIColor(white: 0, alpha: 0.1)
        resizer.translatesAutoresizingMaskIntoConstraints = false
        resizer.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        resizer.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        resizer.widthAnchor.constraint(equalToConstant: GPMainViewController.ResizerWidth).isActive = true
        resizerLeft = resizer.leftAnchor.constraint(equalTo: view.leftAnchor)
        resizerLeft!.isActive = true

        _resize(metricsWidth)
        _resizerUpdateVisibility(displayMode)
        
        // resizing
        
        let recognizer = UIPanGestureRecognizer(target: self, action: #selector(_resized))
        
        resizer.addGestureRecognizer(recognizer)
    }

    override func viewDidLayoutSubviews()
    {
        // metrics
        
        var metricsFrame = metricsView!.collectionView!.frame
        
        metricsFrame.size.height = primary.view.frame.size.height - GPMainViewController.BasementHeight
        
        metricsView!.collectionView!.frame = metricsFrame
        
        // status
        
        var statusFrame = statusController!.view.frame
        
        statusFrame.origin.x = 0
        statusFrame.origin.y = metricsFrame.size.height
        statusFrame.size.width = metricsFrame.size.width
        statusFrame.size.height = GPMainViewController.BasementHeight
        
        statusController!.view.frame = statusFrame
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: - UISplitViewControllerDelegate
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    func splitViewController(_ splitViewController: UISplitViewController,
                             collapseSecondary secondaryViewController: UIViewController,
                             onto primaryViewController: UIViewController) -> Bool
    {
        let mapNav = secondaryViewController as! UINavigationController
        let mapView = mapNav.topViewController as! GPMapViewController
        
        navMetrics.target = self
        navMetrics.action = #selector(onNavMetrics)
        
        mapView.navigationItem.rightBarButtonItems = nil
        mapView.navigationItem.rightBarButtonItem = mapView.navMore
        mapView.navigationItem.leftBarButtonItem = navMetrics
        
        return false
    }

    func splitViewController(_ svc: UISplitViewController,
                             willChangeTo displayMode: UISplitViewControllerDisplayMode)
    {
        _updateMetricsTrigger(displayMode)
    }

}
