# README #

### Green Plane ###

Sample project for Golden Flag America, LLC

Version 1.0

### Specs from client ###

https://www.dropbox.com/s/yxy3wc18ambk7um/GFiOSTestProject.pdf?dl=0

### Workspace ###

You can find it in <repository root>/ios/proj/Green Plane.xcworkspace

Workspace Layout:

* ios - iOS app
* shared - shared libs
* shared/utils - internal handy utilities

### Project ###

* Projects layout:
* code - project code
* db - database for future
* proj - project files
* res - resources (pictures etc)
* test - tests

### iOS Project code structure ###

* 1st folders level - application and screens
* 2nd folders level - screen files and folders containing main views file

### MVVM ###

Lets take /ios/app/Main/Metrics for example

* GPMetric.swift - Model
* GPMetricsController.swift - View Model
* GPMetricsViewController.swift - View
* GPMetricsViewProtocol.swift - View Protocol