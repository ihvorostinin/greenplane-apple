//
//  Location.swift
//  GFUtils
//
//  Created by Ivan Khvorostinin on 23/09/16.
//  Copyright © 2016 Golden Flag America, LLC. All rights reserved.
//

import CoreGraphics
import CoreLocation

public extension CLLocationCoordinate2D
{

    public init(cg: CGPoint)
    {
        self.init(latitude: Double(cg.x), longitude: Double(cg.y))
    }

    public init(polygonCenter: GFPolygon)
    {
        self.init(cg: GFMath.Center(polygonCenter))
    }
    
    var cgpoint: CGPoint
    {
        get
        {
            return CGPoint(x: CGFloat(latitude), y: CGFloat(longitude))
        }
    }
    
    public func shift(_ meters: Double, course: Double) -> CLLocationCoordinate2D
    {
        let distRadians = meters / (6372797.6) // earth radius in meters
        
        let bearing = course * M_PI / 180
        let lat1 = latitude * M_PI / 180
        let lon1 = longitude * M_PI / 180
        
        let lat2 = asin(sin(lat1) * cos(distRadians) + cos(lat1) * sin(distRadians) * cos(bearing))
        let lon2 = lon1 + atan2(sin(bearing) * sin(distRadians) * cos(lat1), cos(distRadians) - sin(lat1) * sin(lat2))
        
        return CLLocationCoordinate2D(latitude: lat2 * 180 / M_PI, longitude: lon2 * 180 / M_PI)
    }

}

public extension CLLocation
{
    
    public convenience init(coordinate: CLLocationCoordinate2D)
    {
        self.init(latitude: coordinate.latitude, longitude: coordinate.longitude)
    }

    public convenience init(coordinate: CLLocationCoordinate2D, course: CLLocationDirection)
    {
        self.init(coordinate: coordinate,
                  altitude: 0,
                  horizontalAccuracy: 0,
                  verticalAccuracy: 0,
                  course: course,
                  speed: 0,
                  timestamp: Date())
    }

    public convenience init(cg: CGPoint)
    {
        self.init(coordinate: CLLocationCoordinate2D(cg: cg))
    }
    
    public func cloneWithCourse(_ course: CLLocationDirection) -> CLLocation
    {
        return CLLocation(coordinate: coordinate,
                          altitude: altitude,
                          horizontalAccuracy: horizontalAccuracy,
                          verticalAccuracy: verticalAccuracy,
                          course: course,
                          speed: speed,
                          timestamp: timestamp)
    }

    public func cloneWithCoordinate(_ coordinate: CLLocationCoordinate2D) -> CLLocation
    {
        return CLLocation(coordinate: coordinate,
                          altitude: altitude,
                          horizontalAccuracy: horizontalAccuracy,
                          verticalAccuracy: verticalAccuracy,
                          course: course,
                          speed: speed,
                          timestamp: timestamp)
    }

    var cgpoint: CGPoint
    {
        get
        {
            return coordinate.cgpoint
        }
    }

}
