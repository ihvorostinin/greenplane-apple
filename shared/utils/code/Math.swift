//
//  Math.swift
//  GFUtils
//
//  Created by Ivan Khvorostinin on 22/09/16.
//  Copyright © 2016 Golden Flag America, LLC. All rights reserved.
//

import Foundation
import CoreGraphics

public typealias GFPolygon = Array<CGPoint>

open class GFMath
{
    
    fileprivate static func _Center(_ count: Int, src: (Int) -> CGFloat) -> CGFloat
    {
        var sum: CGFloat = 0
        
        for i in 0 ..< count
        {
            sum += src(i)
        }
        
        return sum / CGFloat(count)
    }
    
    open static func Center(_ polygon: GFPolygon) -> CGPoint
    {
        let x = _Center(polygon.count) { (i: Int) -> CGFloat in
            
            return polygon[i].x
        }
        
        let y = _Center(polygon.count) { (i: Int) -> CGFloat in
            
            return polygon[i].y
        }
        
        return CGPoint(x: x, y: y)
    }
    
    open static func Rect(_ polygon: GFPolygon) -> CGRect
    {
        var left: CGFloat = CGFloat(Int.max)
        var right: CGFloat = CGFloat(Int.min)
        var top: CGFloat = CGFloat(Int.min)
        var bottom: CGFloat = CGFloat(Int.max)
        
        for i in polygon
        {
            if (i.x < left)
            {
                left = i.x
            }
            
            if (i.x > right)
            {
                right = i.x
            }
            
            if (i.y > top)
            {
                top = i.y
            }
            
            if (i.y < bottom)
            {
                bottom = i.y
            }
        }
        
        return CGRect(x: left, y: bottom, width: right - left, height: top - bottom)
    }
    
}
