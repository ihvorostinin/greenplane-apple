//
//  Types.swift
//  GPUtils
//
//  Created by Ivan Khvorostinin on 14/09/16.
//  Copyright © 2016 Golden Flag America, LLC. All rights reserved.
//

import Foundation

open class GFPair <TKey, TValue>
{
    open var key:TKey
    open var val:TValue?
    
    public init(key:TKey, val:TValue?)
    {
        self.key = key;
        self.val = val;
    }
}
