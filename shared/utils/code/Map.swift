//
//  Map.swift
//  GFUtils
//
//  Created by Ivan Khvorostinin on 23/09/16.
//  Copyright © 2016 Golden Flag America, LLC. All rights reserved.
//

import MapKit

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// MARK: - Annotation
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

open class GFMapAnnotation : NSObject, MKAnnotation
{
    
    fileprivate var _coordinate: CLLocationCoordinate2D!
    fileprivate var _title: String?
    fileprivate var _subtitle: String?

    public init(coordinate: CLLocationCoordinate2D)
    {
        _coordinate = coordinate
    }

    public init(coordinate: CLLocationCoordinate2D, title: String?)
    {
        _coordinate = coordinate
        _title = title
    }

    public init(coordinate: CLLocationCoordinate2D, title: String?, subtitle: String?)
    {
        _coordinate = coordinate
        _title = title
        _subtitle = subtitle
    }
    
    open var coordinate: CLLocationCoordinate2D
    {
        get
        {
            return _coordinate
        }
    }
    
    open var title: String?
    {
        get
        {
            return _title
        }
    }
    
    open var subtitle: String?
    {
        get
        {
            return _subtitle
        }
    }
    
}

public extension GFMapAnnotation
{
    
    public convenience init(center: GFPolygon, title: String)
    {
        self.init(coordinate: CLLocationCoordinate2D(polygonCenter: center),
                  title: title)
    }
    
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// MARK: - Geometry
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

public extension MKMapPoint
{
    
    init(cg: CGPoint)
    {
        x = Double(cg.x)
        y = Double(cg.y)
    }
    
}

public extension MKMapSize
{
    
    init(cg: CGSize)
    {
        width = Double(cg.width)
        height = Double(cg.height)
    }
    
}

public extension MKMapRect
{
    
    init(cg: CGRect)
    {
        origin = MKMapPoint(cg: cg.origin)
        size = MKMapSize(cg: cg.size)
    }
    
}

public extension MKCoordinateRegion
{
    
    public init(polygon: GFPolygon)
    {
        let rect = GFMath.Rect(polygon)
        let center = CLLocationCoordinate2D(polygonCenter: polygon)
        
        // show region
        
        let span = MKCoordinateSpan(latitudeDelta: Double(rect.size.width),
                                    longitudeDelta: Double(rect.size.height))

        self.init(center: center, span: span)
    }
    
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// MARK: - MKPolygon
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

public extension MKPolygon
{
    
    public convenience init(gf: GFPolygon)
    {
        let overlay_coords = UnsafeMutablePointer<CLLocationCoordinate2D>.allocate(capacity: gf.count)
        
        for i in 0 ..< gf.count
        {
            overlay_coords[i] = CLLocationCoordinate2D(latitude: Double(gf[i].x),
                                                       longitude: Double(gf[i].y))
        }
        
        self.init(coordinates: overlay_coords, count: gf.count)
    }
    
}

